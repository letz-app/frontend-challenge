# Letz - Code Challenge

## Objetivo

O objetivo desse desafio é avaliar suas habilidades em áreas relevantes ao desenvolvimento front-end na Letz.

Isso inclui:

- Habilidade de utilizar React/Javascript ao interagir com APIs.
- Conhecimentos em criar e implementar uma interface básica, utilizando boas práticas para oferecer uma boa experiência aos usuários.
- Experiência com Javascript, HTML e CSS para criar um codigo limpo, legível e performático.

**Palavras-chave**
Funcionalidade | Formatação | Estrutura do Projeto | Escalabilidade | Manutenção | Boas práticas de mercado

## Detalhes

O projeto deverá utilizar a PokéAPI - [The RESTful Pokémon API](https://pokeapi.co/) para desenvolver uma aplicação onde se possa listar, buscar e favoritar pokemons.

O uso de bibliotecas externas (Axios, Redux, etc) ou de interface (Bootstrap, Material, etc) é liberado.
Apenas faça bom uso delas, a dica é não esquecer que esse espaço é para mostrar **o que você sabe fazer**.

Não vamos avaliar o design, mas sim a experiência do usuário em todas as suas interações com o app.

## O que preciso fazer?

- [ ] Utilizar [PokéAPI](https://pokeapi.co/)
- [ ] Página de listagem de pokemons
- [ ] Mecanismo de busca por nome dos pokemons
- [ ] Página onde possa listar os pokemons favoritos (não é necessário persistir os dados).
- [ ] Responsividade (desktop/celular)
- [ ] React/Javascript como tecnologia.
- [ ] Testes unitários
- [ ] Readme com instruções de como instalar, fazer funcionar e etc.

## O que vamos avaliar?

- Uso correto do gerenciamento de state
- Boa navegação entre as funcionalidades
- Componentização
- Boas práticas nas chamadas de API e tratamento de dados
- Separação de responsabilidades das lógicas das regras de negócio e interface

## Ponto extras

- Typescript
- Filtros, ordenação e paginação são sempre bem-vindos.
- Um resumo de como foi feito o desenvolvimento, bem como um plano para as implementações que gostaria de ter feito.

## Como entregar

Crie um repositório público (onde você preferir e compartilhe o link com a gente).
